package main

import (
	"encoding/xml"
	"net/http"
	"time"
)

const (
	baseurl = "http://atom-updater.appspot.com"
	title   = "ATOM Connection Launcher Update"
)

func init() {
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/xml")

	version := "1.0.34.0"
	pubDate := time.Now()

	rss, err := NewSparkleRSS(baseurl, title, version, pubDate)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	enc := xml.NewEncoder(w)
	err = enc.Encode(rss)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//func main() {
//}
