package main

import (
	"fmt"
	"time"
)

func NewSparkleRSS(baseurl string, title string, version string, pubDate time.Time) (*SparkleRSS, error) {
	rss := SparkleRSS{
		Version: "2.0",
		XMLNS:   "http://www.andymatuschak.org/xml-namespaces/sparkle",
		XMLDC:   "http://purl.org/dc/elements/1.1/",
		Channel: SparkleChannel{
			Title:       title,
			Link:        baseurl + "/versioninfo.xml",
			Description: title,
			Language:    "en",
			Item: SparkleItem{
				Title:            fmt.Sprintf("Version %s", version),
				ReleaseNotesLink: baseurl + fmt.Sprintf("/%s/Release.html", version),
				PubDate:          pubDate,
				Enclosure: SparkleItemEnclosure{
					Url:     baseurl + fmt.Sprintf("/%s/Setup.exe", version),
					Type:    "application/octet-stream",
					Version: version,
				},
			},
		},
	}

	return &rss, nil
}

type SparkleRSS struct {
	XMLName string         `xml:"rss"`
	Version string         `xml:"version,attr"`
	XMLNS   string         `xml:"xmlns:sparkle,attr"`
	XMLDC   string         `xml:"xmlns:dc,attr"`
	Channel SparkleChannel `xml:"channel"`
}

type SparkleChannel struct {
	Title       string      `xml:"title"`
	Link        string      `xml:"link"`
	Description string      `xml:"description"`
	Language    string      `xml:"language"`
	Item        SparkleItem `xml:"item"`
}

type SparkleItem struct {
	Title            string               `xml:"title"`
	ReleaseNotesLink string               `xml:"sparkle:releaseNotesLink"`
	PubDate          time.Time            `xml:"pubDate"`
	Enclosure        SparkleItemEnclosure `xml:"enclosure"`
}

type SparkleItemEnclosure struct {
	Url     string `xml:"url,attr"`
	Type    string `xml:"type,attr"`
	Version string `xml:"sparkle:version,attr"`
}
